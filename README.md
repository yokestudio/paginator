# Paginator #

A JavaScript library for pagination buttons

Essentially, do all the arithmetic and generates page buttons for your application. All you need to do is pass in an element to contain the buttons (`nav` or `div` or whatever), and tell it how many pages there are in total and what page you are currently on.

### Features ###
Customisation to show speicla page buttons, e.g. show first page, last page, go back 5 pages, go back 10 pages, etc.

### Browser Support ###
* IE 9+
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of ?? ??? 2016, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="paginator-1.0.0.min.js"></script>`

3. Ensure CSS styles for these elements are done:
      - `.page-button`            // defaults to button style
      - `.spacer`                 // defaults to div style

4. Optionally, also set these styles (if different from page-button):
      - `.first-button`           // defaults to button style
      - `.last-button`           // defaults to button style
      - `.prev-button`           // defaults to button style
      - `.next-button`           // defaults to button style

### API ###
**`init`**

```
/**
 * Generate a list of buttons for a number of pages.
 * - All generated page buttons have class "page-button"
 * - The current page will also have class "current"
 * - A "prev-button" will be prepended to the page buttons.
 * - A "next-button" will be appended to the page buttons.
 * @param: {HTMLElement} the element to contain pagination buttons
 *         {object} options
 *                 - {number} pageCount
 *                 - {number} currPage
 *                 - {number} maxButtons - maximum number of buttons to show (excluding prev, next, first, last) (DEFAULT: 9)
 *                 - {bool} showFirst    - whether to show a button for first page       (DEFAULT: true)
 *                 - {bool} showLast     - whether to show a button for last page        (DEFAULT: true)
 *                 - {bool} showPrev     - whether to show a button for previous page    (DEFAULT: true)
 *                 - {bool} showNext     - whether to show a button for next page        (DEFAULT: true)
 *                 - {bool} showPrev5    - whether to show a button for 5 pages back     (DEFAULT: true)
 *                 - {bool} showNext5    - whether to show a button for 5 pages forward  (DEFAULT: true)
 *                 - {bool} showPrev10   - whether to show a button for 10 pages back    (DEFAULT: false)
 *                 - {bool} showNext10   - whether to show a button for 10 pages forward (DEFAULT: false)
 *                 - 
 */

// Sample usage
var div = document.getElementById('#pagination');
paginator.init(div, {
	currPage: 2,
	maxPages: 15,
	maxButtons: 7
});
```

### FAQ ###

1. What's wrong with the current options?
> There are no real good ones, as far as we could tell. Some are not styleable, and some are dependent on jQuery.

2. Why do the buttons not come with any styles?
> Styles should always be separate from content/behaviour as far as possible. We believe it's best to leave all styling work to CSS. A default stylesheet is included in this repository for reference.

3. What is a spacer?
> An element to add space between the page buttons and the `first-button` / `last-button`, if the page numbers are not consecutive. In many cases, they are styled to show ellipsis (`...`). For example: `[1 ... 6 7 8 9 ... 24]` contains two spacers - one between `1` and `6`, the other between `9` and `24`.

3. Why do the special page buttons not contain any text?
> Some people prefer arrows on the prev/next buttons; some prefer the words "PREV" and "NEXT"; some like "BACK" and "FORWARD"; some need to show in a non-English language. It's easy to style them separately through CSS pseudo-selectors (`::before` and `::after`) or using graphics (as `background-image`). The possibilities are endless, and we don't want to make any assumptions or hard-code any unnecessary painted corners.

### Contact ###

* Email us at <support@yokestudio.com>.