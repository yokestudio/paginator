/***
 * Library - Paginator
 *
 * JS Widget that adds CSS-customisable pagination buttons to app.
 *
 * * Before use:
 *   - Ensure CSS styles for page button elements are done:
 *     - .page-button           // defaults to button style
 *     - .spacer                // defaults to div style
 *
 *   - Optionally, also set these styles (if different from page-button):
 *     - .first-button          // defaults to button style
 *     - .last-button           // defaults to button style
 *     - .prev-button           // defaults to button style
 *     - .next-button           // defaults to button style
 ***/
(function(global) {
	'use strict';

	var DEFAULT_OPTIONS = {
		'pageCount': 1,
		'currPage': 1,
		'maxButtons': 9,
		'showFirst': true,
		'showLast': true,
		'showPrev': true,
		'showNext': true,
		'showPrev5': true,
		'showNext5': true,
		'showPrev10': false,
		'showNext10': false,
		'spacerText': '',
		'prevButtonText': '',
		'nextButtonText': '',
		'prev5ButtonText': '',
		'next5ButtonText': '',
		'prev10ButtonText': '',
		'next10ButtonText': '',
		'onButtonClick': function() { /* do nothing*/ }
	};

	/**
	 * Generate a list of buttons for a number of pages.
	 * - All generated page buttons have class "page-button"
	 * - The current page will also have class "current"
	 * - A "prev-button" will be prepended to the page buttons.
	 * - A "next-button" will be appended to the page buttons.
	 * @param: {HTMLElement} the element to contain pagination buttons
	 *         {object} opts
	 *                 - {number} pageCount
	 *                 - {number} currPage
	 *                 - {number} maxButtons - maximum number of buttons to show (excluding prev, next, first, last) (DEFAULT: 9)
	 *                 - {bool} showFirst    - whether to show a button for first page       (DEFAULT: true)
	 *                 - {bool} showLast     - whether to show a button for last page        (DEFAULT: true)
	 *                 - {bool} showPrev     - whether to show a button for previous page    (DEFAULT: true)
	 *                 - {bool} showNext     - whether to show a button for next page        (DEFAULT: true)
	 *                 - {bool} showPrev5    - whether to show a button for 5 pages back     (DEFAULT: true)
	 *                 - {bool} showNext5    - whether to show a button for 5 pages forward  (DEFAULT: true)
	 *                 - {bool} showPrev10   - whether to show a button for 10 pages back    (DEFAULT: false)
	 *                 - {bool} showNext10   - whether to show a button for 10 pages forward (DEFAULT: false)
	 *                 - {string} spacerText - (DEFAULT: '')
	 *                 - {string} prevButtonText - (DEFAULT: '')
	 *                 - {string} nextButtonText - (DEFAULT: '')
	 *                 - {string} prev5ButtonText - (DEFAULT: '')
	 *                 - {string} next5ButtonText - (DEFAULT: '')
	 *                 - {string} prev10ButtonText - (DEFAULT: '')
	 *                 - {string} next10ButtonText - (DEFAULT: '')
	 *                 - {function} onButtonClick - additional callback to execute. Function accepts 1 arg {int} denoting page number
	 */
	function init(dom_elem, opts) {
		if (!(dom_elem instanceof HTMLElement)) {
			throw new TypeError('paginator.init() - expects HTMLElement. ' + typeof dom_elem);
		}
		var options;
		if (typeof opts === 'object') {
			options = Object.assign({}, opts);
		}
		else {
			options = Object.assign({}, DEFAULT_OPTIONS);
		}

		// Process options
		for (var key in DEFAULT_OPTIONS) {
			if (!DEFAULT_OPTIONS.hasOwnProperty(key)) {
				continue;
			}

			if (typeof DEFAULT_OPTIONS[key] !== typeof options[key]) {
				options[key] = DEFAULT_OPTIONS[key];
			}
		}
		if (options['pageCount'] < options['currPage']) {
			throw new RangeError('paginator.init() - currPage (' + options['currPage'] + ') cannot be more than pageCount (' + options['pageCount'] + ')');
		}

		generateButtonsHTML(dom_elem, options);
	} // init()

	/**
	 * @private
	 */
	function generateButtonsHTML(dom_elem, options) {
		var i, j, len; // loop vars

		// Show page buttons
		var buttons_html;
		if (options['pageCount'] === 0) {
			buttons_html = '';
		} // Trivial case: no pages
		else if (options['pageCount'] === 1) {
			buttons_html = '<button type="button" class="page-button current" data-page="1">1</button>';
		} // Trivial case: 1 page
		else {
			buttons_html = '<button type="button" class="page-button current" data-page="' + options['currPage'] + '">' +
			                   options['currPage'] +
			               '</button>';

			var total_button_count = Math.min(options['maxButtons'], options['pageCount'] - 2); // eslint-disable-line no-magic-numbers
			var curr_button_count = 1;
			if (options['currPage'] === 1 && options['showFirst']) {
				curr_button_count = 0; // do not double-count
			}
			if (options['currPage'] === options['pageCount'] && options['showLast']) {
				curr_button_count = 0; // do not double-count
			}

			// Add buttons until we have the total count we need
			i = options['currPage'] - 1;
			j = options['currPage'] + 1;
			while (curr_button_count < total_button_count) {
				// Add on right side
				if (j < options['pageCount']) {
					buttons_html += '<button type="button" class="page-button" data-page="' + j + '">' +
					                    j +
					                '</button>';
					curr_button_count++;
					j++;
					if (curr_button_count === total_button_count) {
						break;
					}
				}

				// Add on left side
				if (i > 1) {
					buttons_html = '<button type="button" class="page-button" data-page="' + i + '">' + i + '</button>' + buttons_html;
					curr_button_count++;
					i--;
					if (curr_button_count === total_button_count) {
						break;
					}
				}
			} // while we need more buttons

			// First page
			if (options['showFirst']) {
				if (i > 1) {
					buttons_html = '<div class="spacer">' + options['spacerText'] + '</div>' + buttons_html;
				}
				if (options['currPage'] !== 1) {
					buttons_html = '<button type="button" class="page-button first-button" data-page="1">1</button>' + buttons_html;
				}
			}

			// Last page
			if (options['showLast']) {
				if (j < options['pageCount']) {
					buttons_html = buttons_html + '<div class="spacer">' + options['spacerText'] + '</div>';
				}
				if (options['currPage'] !== options['pageCount']) {
					buttons_html = buttons_html + '<button type="button" class="page-button last-button" data-page="' + options['pageCount'] + '">' + options['pageCount'] + '</button>';
				}
			}
		} // Multiple pages

		var target_page; // for prev / next button

		// Prev Button
		if (options['showPrev']) {
			target_page = options['currPage'] - 1;
			if (options['currPage'] > 1 && options['pageCount'] > 1) {
				buttons_html = '<button type="button" class="page-button prev-button" data-page="' + target_page + '">' + options['prevButtonText'] + '</button>' + buttons_html;
			}
			else {
				buttons_html = '<button type="button" class="page-button prev-button" disabled="disabled">' + options['prevButtonText'] + '</button>' + buttons_html;
			}
		}

		// Prev 5 Button
		if (options['showPrev5']) {
			target_page = options['currPage'] - 5; // eslint-disable-line no-magic-numbers
			if (target_page < 1) {
				target_page = 1;
			}
			if (options['currPage'] > 1 && options['pageCount'] > 1) {
				buttons_html = '<button type="button" class="page-button prev5-button" data-page="' + target_page + '">' + options['prev5ButtonText'] + '</button>' + buttons_html;
			}
			else {
				buttons_html = '<button type="button" class="page-button prev5-button" disabled="disabled">' + options['prev5ButtonText'] + '</button>' + buttons_html;
			}
		}

		// Prev 10 Button
		if (options['showPrev10']) {
			target_page = options['currPage'] - 10; // eslint-disable-line no-magic-numbers
			if (target_page < 1) {
				target_page = 1;
			}
			if (options['currPage'] > 1 && options['pageCount'] > 1) {
				buttons_html = '<button type="button" class="page-button prev10-button" data-page="' + target_page + '">' + options['prev10ButtonText'] + '</button>' + buttons_html;
			}
			else {
				buttons_html = '<button type="button" class="page-button prev10-button" disabled="disabled">' + options['prev10ButtonText'] + '</button>' + buttons_html;
			}
		}

		// Next Button
		if (options['showNext']) {
			target_page = options['currPage'] + 1;
			if (options['currPage'] < options['pageCount'] && options['pageCount'] > 1) {
				buttons_html = buttons_html + '<button type="button" class="page-button next-button" data-page="' + target_page + '">' + options['nextButtonText'] + '</button>';
			}
			else {
				buttons_html = buttons_html + '<button type="button" class="page-button next-button" disabled="disabled">' + options['nextButtonText'] + '</button>';
			}
		}

		// Next 5 Button
		if (options['showNext5']) {
			target_page = options['currPage'] + 5; // eslint-disable-line no-magic-numbers
			if (target_page > options['pageCount']) {
				target_page = options['pageCount'];
			}
			if (options['currPage'] < options['pageCount'] && options['pageCount'] > 1) {
				buttons_html = buttons_html + '<button type="button" class="page-button next5-button" data-page="' + target_page + '">' + options['next5ButtonText'] + '</button>';
			}
			else {
				buttons_html = buttons_html + '<button type="button" class="page-button next5-button" disabled="disabled">' + options['next5ButtonText'] + '</button>';
			}
		}

		// Next 10 Button
		if (options['showNext10']) {
			target_page = options['currPage'] + 10; // eslint-disable-line no-magic-numbers
			if (target_page > options['pageCount']) {
				target_page = options['pageCount'];
			}
			if (options['currPage'] < options['pageCount'] && options['pageCount'] > 1) {
				buttons_html = buttons_html + '<button type="button" class="page-button next10-button" data-page="' + target_page + '">' + options['next10ButtonText'] + '</button>';
			}
			else {
				buttons_html = buttons_html + '<button type="button" class="page-button next10-button" disabled="disabled">' + options['next10ButtonText'] + '</button>';
			}
		}

		dom_elem.innerHTML = buttons_html;

		// Bind button listeners
		var dom_buttons = [].slice.call(dom_elem.querySelectorAll('button'));
		for (i = 0, len = dom_buttons.length; i < len; i++) {
			dom_buttons[i].addEventListener('click', onButtonClick);
		}

		function onButtonClick(e) {
			// Find out which page was clicked
			var pg = parseInt(e.currentTarget.getAttribute('data-page'));

			// Unbind button listeners
			for (i = 0, len = dom_buttons.length; i < len; i++) {
				dom_buttons[i].removeEventListener('click', onButtonClick);
			}

			// Regenerate buttons for this page
			options['currPage'] = pg;
			generateButtonsHTML(dom_elem, options);

			return options['onButtonClick'](pg);
		}
	} // generateButtonsHTML()

	global['paginator'] = {
		'init': init
	};
}(this));
